package uz.blog.interfaceHelpers;

import uz.blog.helpers.enums.ValidationResult;
import uz.blog.model.users.User;

import java.util.function.Function;

public interface UserRegistrationValidator extends Function<User, ValidationResult> {


    static UserRegistrationValidator isEmailValid() {
        return user -> user.getEmail().contains("@") ? ValidationResult.SUCCESS : ValidationResult.EMAIL_NOT_VALID;
    }

    static UserRegistrationValidator isPasswordValid() {
        return user -> user.getPassword().length() >= 8 ? ValidationResult.SUCCESS : ValidationResult.PASSWORD_NOT_VALID;
    }

    default UserRegistrationValidator and(UserRegistrationValidator other) {
        return user -> {
            ValidationResult result = this.apply(user);
            return result.equals(ValidationResult.SUCCESS) ? other.apply(user) : result;
        };
    }


}
