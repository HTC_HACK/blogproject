package uz.blog.interfaceHelpers.builder;

import uz.blog.helpers.enums.RoleEnum;
import uz.blog.model.users.User;

@FunctionalInterface
public interface UserBuilder {

    User create(Integer id, String name, String email, String password, RoleEnum role, double balance);

}
