package uz.blog.interfaceHelpers.builder;

import uz.blog.helpers.enums.PostStatus;
import uz.blog.model.posts.Post;
import uz.blog.model.users.User;

public interface PostBuilder {

    Post create(Integer id, String title, String description, PostStatus status, User user);
}
