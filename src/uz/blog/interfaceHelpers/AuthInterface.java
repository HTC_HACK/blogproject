package uz.blog.interfaceHelpers;

import uz.blog.model.users.User;

public interface AuthInterface {

    void signUp();

    User signIn();

    void accountInfo(User user);

    void users(User user);

    public void addAdmin(User user);

    public void recoverUser(User user);

}
