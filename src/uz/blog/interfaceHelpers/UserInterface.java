package uz.blog.interfaceHelpers;

import uz.blog.model.posts.Post;
import uz.blog.model.users.User;

public interface UserInterface {

    void addPost(User user);

    void myPost(User user);

    void editPost(User user);

    void deletePost(User user);

    void posts();

    void postStatus();

    void postDetail(Post post);


}
