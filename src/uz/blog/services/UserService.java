package uz.blog.services;

import uz.blog.helpers.enums.PostStatus;
import uz.blog.helpers.messages.MessageHelper;
import uz.blog.interfaceHelpers.UserInterface;
import uz.blog.interfaceHelpers.builder.PostBuilder;
import uz.blog.model.posts.Post;
import uz.blog.model.users.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;

public class UserService implements UserInterface {

    List<Post> posts = new ArrayList<>();
    int postId = 2000;

    Scanner scannerStr = new Scanner(System.in);
    Scanner scannerInt = new Scanner(System.in);


    @Override
    public void addPost(User user) {
        System.out.println("Enter post title");
        String title = scannerStr.nextLine();

        System.out.println("Enter description");
        String description = scannerStr.nextLine();

        PostBuilder postBuilder = Post::new;

        posts.add(postBuilder.create(postId++, title, description, PostStatus.DRAFT, user));
    }

    @Override
    public void myPost(User user) {
        posts.stream().filter(post -> post.getUser().equals(user)).forEach(System.out::println);
        System.out.println("Enter post id");
        int postId = scannerInt.nextInt();
        posts.stream().filter(post -> post.getId() == postId).forEach(post -> System.out.println(post.postDetail(post)));
    }

    @Override
    public void editPost(User user) {
        myPost(user);
        System.out.println("Enter post id");
        int postEdit = scannerInt.nextInt();

        Predicate<Post> postPredicate = post -> post.getId() == postEdit && post.getUser().equals(user);

        for (Post post : posts) {
            if (postPredicate.test(post)) {
                System.out.println("Enter title");
                String title = scannerStr.nextLine();
                post.setTitle(title);

                System.out.println("Enter description");
                String description = scannerStr.nextLine();
                post.setDescription(description);
                System.out.println(MessageHelper.SUCCESS);
                return;
            }
        }

        System.out.println(MessageHelper.POST_NOT_FOUND);
    }

    @Override
    public void deletePost(User user) {
        myPost(user);
        System.out.println("Enter post id");
        int postDel = scannerInt.nextInt();

        Predicate<Post> postPredicate = post -> post.getId() == postDel && post.getUser().equals(user);
        boolean result = posts.removeIf(postPredicate::test);
        if (result)
            System.out.println(MessageHelper.DELETED);
        else
            System.out.println(MessageHelper.ERROR);
    }

    @Override
    public void posts() {
        posts.stream().filter(post -> post.getStatus().equals(PostStatus.CONFIRM)).forEach(System.out::println);
        System.out.println("Enter post id or back 0");
        int postId = scannerInt.nextInt();
        if (postId == 0)
            return;
        for (Post post : posts) {
            if (post.getId() == postId)
                System.out.println(post.postDetail(post));
        }
    }

    @Override
    public void postStatus() {
        posts.stream().filter(post -> post.getStatus().equals(PostStatus.DRAFT)).forEach(System.out::println);
        System.out.println("Enter post id");
        int postId = scannerInt.nextInt();

        Post postResult = posts.stream().filter(post -> post
                        .getStatus()
                        .equals(PostStatus.DRAFT) && post.getId() == postId)
                .findFirst().orElse(null);

        if (postResult != null) {
            posts.stream().filter(post -> post.equals(postResult)).forEach(post -> System.out.println(post.postDetail(post)));
            System.out.println("1=>" + PostStatus.CONFIRM + " 2=>" + PostStatus.REJECT + " 3=>BACK");
            int option = scannerInt.nextInt();
            PostStatus status = PostStatus.DRAFT;

            switch (option) {
                case 1:
                    status = PostStatus.CONFIRM;
                    break;
                case 2:
                    status = PostStatus.REJECT;
                    break;
                case 3:
                    break;
                default:
                    status = PostStatus.DRAFT;
                    break;
            }

            posts.stream()
                    .filter(post -> post.equals(postResult))
                    .findFirst()
                    .orElse(null).setStatus(status);

            System.out.println(MessageHelper.SUCCESS);
            return;
        }

        System.out.println(MessageHelper.ERROR);
    }

    @Override
    public void postDetail(Post postIn) {
        posts.stream().filter(post -> post.equals(postIn)).forEach(post -> post.postDetail(post));
    }

}