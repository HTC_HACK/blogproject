package uz.blog.services;

import uz.blog.helpers.enums.RoleEnum;
import uz.blog.helpers.enums.ValidationResult;
import uz.blog.helpers.messages.MessageHelper;
import uz.blog.interfaceHelpers.AuthInterface;
import uz.blog.interfaceHelpers.builder.UserBuilder;
import uz.blog.model.users.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static uz.blog.interfaceHelpers.UserRegistrationValidator.isEmailValid;
import static uz.blog.interfaceHelpers.UserRegistrationValidator.isPasswordValid;

public class AuthService implements AuthInterface {

    List<User> users = new ArrayList<>();
    int userid = 1000;

    Scanner scannerStr = new Scanner(System.in);
    Scanner scannerInt = new Scanner(System.in);

    public AuthService() {
        UserBuilder userBuilder = User::new;
        users.add(userBuilder.create(999,
                "admin",
                "admin@gmail.com",
                "admin@admin",
                RoleEnum.SUPER_ADMIN,
                20000));

    }

    @Override
    public void signUp() {

        System.out.println("Enter name");
        String name = scannerStr.nextLine();

        System.out.println("Enter email");
        String email = scannerStr.nextLine();

        System.out.println("Enter password");
        String password = scannerStr.nextLine();

        if (!checkEmail(email)) {
            UserBuilder userBuilder = User::new;
            User user = userBuilder.create(userid, name, email, password, RoleEnum.USER, 5000);

            ValidationResult result = isEmailValid()
                    .and(isPasswordValid())
                    .apply(user);

            if (result.equals(ValidationResult.SUCCESS)) {
                users.add(user);
                userid++;
            }
            System.out.println(result);
        } else
            System.out.println(ValidationResult.EMAIL_EXIST);

    }

    private boolean checkEmail(String email) {
        for (User user : users) if (user.getEmail().equals(email)) return true;
        return false;
    }

    @Override
    public User signIn() {
        User userInput = new User();
        System.out.println("Enter email");
        String email = scannerStr.nextLine();
        userInput.setEmail(email);

        System.out.println("Enter password");
        String password = scannerStr.nextLine();
        userInput.setPassword(password);


        return users.stream().filter(user ->
                user.getEmail().equals(userInput.getEmail()) && user.getPassword().equals(userInput.getPassword())
        ).findFirst().orElse(null);
    }

    @Override
    public void accountInfo(User user) {
        users.stream()
                .filter(user1 -> user1.equals(user)).forEach(System.out::println);
    }

    @Override
    public void users(User user) {
        users.stream()
                .forEach(System.out::println);
    }

    @Override
    public void addAdmin(User user) {
        if (user.getRole().equals(RoleEnum.SUPER_ADMIN)) {
            System.out.println("Enter name");
            String name = scannerStr.nextLine();
            System.out.println("Enter email");
            String email = scannerStr.nextLine();
            System.out.println("Enter password");
            String password = scannerStr.nextLine();

            if (!checkEmail(email)) {
                UserBuilder userBuilder = User::new;
                User userAdmin = userBuilder.create(userid, name, password, password, RoleEnum.ADMIN, 5000);

                ValidationResult result = isEmailValid()
                        .and(isPasswordValid())
                        .apply(user);

                if (result.equals(ValidationResult.SUCCESS)) {
                    users.add(user);
                    userid++;
                }
                System.out.println(result);
            } else
                System.out.println(ValidationResult.EMAIL_EXIST);
        } else {
            System.out.println(MessageHelper.PERMISSION_DENIED);
        }
    }

    @Override
    public void recoverUser(User user) {

    }
}
