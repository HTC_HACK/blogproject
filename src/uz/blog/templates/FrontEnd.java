package uz.blog.templates;

import uz.blog.helpers.enums.RoleEnum;
import uz.blog.helpers.messages.MessageHelper;
import uz.blog.model.users.User;
import uz.blog.services.AuthService;
import uz.blog.services.UserService;

import java.util.Scanner;

public class FrontEnd {

    Scanner scannerStr = new Scanner(System.in);
    Scanner scannerInt = new Scanner(System.in);

    public void frontEnd() {

        AuthService authService = new AuthService();
        UserService userService = new UserService();

        boolean frontLoop = true;
        while (frontLoop) {
            System.out.println("1=>SignIn. 2=>SignUp. 3=>Exit");
            int optionMenu = scannerInt.nextInt();
            switch (optionMenu) {
                case 1:
                    User user = authService.signIn();
                    if (user != null && (user.getRole().equals(RoleEnum.ADMIN) || user.getRole().equals(RoleEnum.SUPER_ADMIN))) {
                        boolean adminMenu = true;
                        while (adminMenu) {
                            System.out.println("1=>Users. 2=>Posts. 3=>New Posts. 4=>Add Admin. 5=>Edit Post. 6=>Delete Post 7=>Add Post. 8=>Account Info. 9=>My Posts 10=>Logout");
                            int adminOption = scannerInt.nextInt();
                            switch (adminOption) {
                                case 1:
                                    authService.users(user);
                                    break;
                                case 2:
                                    userService.posts();
                                    break;
                                case 3:
                                    userService.postStatus();
                                    break;
                                case 4:
                                    authService.addAdmin(user);
                                    break;
                                case 5:
                                    userService.editPost(user);
                                    break;
                                case 6:
                                    userService.deletePost(user);
                                    break;
                                case 7:
                                    userService.addPost(user);
                                    break;
                                case 8:
                                    authService.accountInfo(user);
                                    break;
                                case 9:
                                    userService.myPost(user);
                                case 10:
                                    adminMenu = false;
                                    break;
                                default:
                                    System.out.println(MessageHelper.WRONG_OPTION);
                                    break;

                            }
                        }
                    } else if (user != null && user.getRole().equals(RoleEnum.USER)) {
                        boolean userMenu = true;
                        while (userMenu) {
                            System.out.println("1=>Add Post. 2=>Posts. 3=>Edit Post. 4=>Delete Post 5=>My Posts. 6=>Account Info. 7=>Logout");
                            int adminOption = scannerInt.nextInt();
                            switch (adminOption) {
                                case 1:
                                    userService.addPost(user);
                                    break;
                                case 2:
                                    userService.posts();
                                    break;
                                case 3:
                                    userService.editPost(user);
                                    break;
                                case 4:
                                    userService.deletePost(user);
                                    break;
                                case 5:
                                    userService.myPost(user);
                                    break;
                                case 6:
                                    authService.accountInfo(user);
                                    break;
                                case 7:
                                    userMenu = false;
                                    break;
                                default:
                                    System.out.println(MessageHelper.WRONG_OPTION);

                            }
                        }
                        break;
                    } else {
                        System.out.println(MessageHelper.USER_NOT_FOUND);
                    }
                    break;
                case 2:
                    authService.signUp();
                    break;
                case 3:
                    frontLoop = false;
                    break;
                default:
                    System.out.println(MessageHelper.WRONG_OPTION);
                    break;
            }
        }
    }

}
