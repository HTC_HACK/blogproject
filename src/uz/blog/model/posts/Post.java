package uz.blog.model.posts;

import uz.blog.helpers.enums.PostStatus;
import uz.blog.model.users.User;

public class Post {

    private Integer id;
    private String title;
    private String description;
    private PostStatus status;
    private User user;

    public Post() {
    }

    public Post(Integer id, String title, String description, PostStatus status, User user) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.status = status;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PostStatus getStatus() {
        return status;
    }

    public void setStatus(PostStatus status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String postDetail(Post post) {
        return
                "[ id=" + post.getId() +
                        " | Title='" + post.getTitle() + '\'' +
                        " | Status=" + post.getStatus() +
                        "\t| Author=" + post.getUser().getName() +
                        "\t| Description=" + post.getDescription() +
                        ']';
    }

    @Override
    public String toString() {
        return
                "[ id=" + id +
                        " | Title='" + title + '\'' +
                        " | Status=" + status +
                        "\t| Author=" + user.getName() +
                        ']';
    }
}
