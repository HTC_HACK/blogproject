package uz.blog.helpers.enums;

public enum ValidationResult {

    SUCCESS,
    PASSWORD_NOT_VALID,
    EMAIL_NOT_VALID,
    EMAIL_EXIST
}
