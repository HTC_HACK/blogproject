package uz.blog.helpers.enums;

public enum RoleEnum {

    USER,
    ADMIN,
    SUPER_ADMIN

}
