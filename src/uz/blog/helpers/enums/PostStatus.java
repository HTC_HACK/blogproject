package uz.blog.helpers.enums;

public enum PostStatus {

    CONFIRM,
    REJECT,
    DRAFT
}
