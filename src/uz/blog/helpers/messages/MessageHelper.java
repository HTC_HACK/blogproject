package uz.blog.helpers.messages;

public enum MessageHelper {

    DELETED,
    ERROR,
    POST_NOT_FOUND,
    SUCCESS,
    PERMISSION_DENIED,
    USER_NOT_FOUND,
    WRONG_OPTION


}
